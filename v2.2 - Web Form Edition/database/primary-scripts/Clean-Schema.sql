﻿-- =============================================
-- Script Template
-- =============================================

-- dropping the table
drop table Employees

-- dropping the stored procedures
drop procedure Custom_Employees_GetAllEmployeeBosses
drop procedure Custom_Employees_GetEmployeeBossByEmployeeId
drop procedure Custom_Employees_GetEmployeeFirstNameByEmployeeId
drop procedure Custom_Employees_GetTwoLevelBosses
drop procedure Employees_CreateNewEmployee
drop procedure Employees_DeleteEmployee
drop procedure Employees_DeleteEmployees
drop procedure Employees_GetAllEmployees
drop procedure Employees_GetAllEmployees_Paged
drop procedure Employees_GetAllEmployees_Paged_Count
drop procedure Employees_GetEmployeeByEmployeeId
drop procedure Employees_GetEmployeesByReportsTo
drop procedure Employees_GetEmployeesByReportsTo_Paged
drop procedure Employees_GetEmployeesByReportsTo_Paged_Count
drop procedure Employees_UpdateEmployee
