
namespace Utilities
{
    /// <summary>
    /// Design and Architecture: Mohammad Ashraful Alam [ashraf@mvps.org]
    /// </summary>
    public enum GenerationCommandType
    { 
        ScalarValue,
        CustomEntitySingleRecord,
        CustomEntityTabular,
        CustomEntityTabularCollection,
        GenericDataTable,
        GenericDataSet,
        WriteReturnSuccess,
        WriteReturnPrimaryKey
    }
}
